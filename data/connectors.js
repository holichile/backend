import Sequelize from 'sequelize'

const db = new Sequelize('HOLI', 'root', 'test', {
  dialect: 'mysql'
})

const ParametrosModel = db.define('ADM_PARametros', { // eslint-disable-line
  parIndice: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  parId: { type: Sequelize.INTEGER, allowNull: false },
  parCodigo: { type: Sequelize.INTEGER, allowNull: false },
  parDescrip1: { type: Sequelize.STRING, allowNull: false },
  parDescrip2: { type: Sequelize.STRING, allowNull: false },
  parObs: { type: Sequelize.STRING, allowNull: false },
  parOrden: { type: Sequelize.INTEGER, allowNull: false },
  parEstado: { type: Sequelize.INTEGER, allowNull: false },
  parFechaModif: { type: Sequelize.DATE, allowNull: false },
  parIdModif: { type: Sequelize.INTEGER, allowNull: false }
}, {
  paranoid: true
})

const EspecialistaModel = db.define('HOLI_ESPecialistas', {
  espId: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  espRut: { type: Sequelize.INTEGER, allowNull: true },
  espDv: { type: Sequelize.STRING, allowNull: true },
  espNombres: { type: Sequelize.STRING, allowNull: true },
  espApePat: { type: Sequelize.STRING, allowNull: true },
  espApeMat: { type: Sequelize.STRING, allowNull: true },
  espFonoCasa: { type: Sequelize.STRING, allowNull: true },
  espFonoEmpresa: { type: Sequelize.STRING, allowNull: true },
  espCelular: { type: Sequelize.STRING, allowNull: true },
  espFecNac: { type: Sequelize.DATE, allowNull: true },
  espEmail: { type: Sequelize.STRING, allowNull: true },
  espDireccion: { type: Sequelize.STRING, allowNull: true },
  espComuna: { type: Sequelize.INTEGER, allowNull: true },
  espObs: { type: Sequelize.BLOB, allowNull: true },
  espIdIngresa: { type: Sequelize.INTEGER, allowNull: true },
  espFechaIngreso: { type: Sequelize.DATE, allowNull: true },
  espContactado: { type: Sequelize.INTEGER, allowNull: true },
  espIngresadoDesde: { type: Sequelize.STRING, allowNull: true },
  espUrlFlayer: { type: Sequelize.STRING, allowNull: true },
  espPaginaWeb: { type: Sequelize.STRING, allowNull: true },
  espEstado: { type: Sequelize.INTEGER, allowNull: true }
}, {
  paranoid: true
})

const CategoriaModel = db.define('HOLI_CATegoria', { // eslint-disable-line
  catId: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  catNombre: { type: Sequelize.STRING, allowNull: true },
  catDescripcion: { type: Sequelize.BLOB, allowNull: true },
  catIcono: { type: Sequelize.STRING, allowNull: true },
  catIdIngresa: { type: Sequelize.INTEGER, allowNull: true },
  catFechaIngreso: { type: Sequelize.DATE, allowNull: true },
  catEstado: { type: Sequelize.INTEGER, allowNull: true },
  catImagen: { type: Sequelize.STRING, allowNull: true }
}, {
  paranoid: true
})

const AgendaModel = db.define('HOLI_Agenda', { // eslint-disable-line
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  eseObs: { type: Sequelize.STRING, allowNull: true },
  empIngresa: { type: Sequelize.STRING, allowNull: true },
  eseEstado: { type: Sequelize.INTEGER, allowNull: true },
  horaInicio: { type: Sequelize.DATE, allowNull: true },
  horaFin: { type: Sequelize.DATE, allowNull: true },
  centro: { type: Sequelize.INTEGER, allowNull: true },
  espId: { type: Sequelize.INTEGER, allowNull: true },
  catId: { type: Sequelize.INTEGER, allowNull: true }
}, {
  paranoid: true
})

const CentroModel = db.define('HOLI_Centros', { // eslint-disable-line
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  nombre: { type: Sequelize.STRING, allowNull: true },
  comuna: { type: Sequelize.INTEGER, allowNull: true },
  direccion: { type: Sequelize.STRING, allowNull: true }
})

db.sync()

const Parametros = db.models.ADM_PARametros
const Especialista = db.models.HOLI_ESPecialistas
const Categoria = db.models.HOLI_CATegoria
const Agenda = db.models.HOLI_Agenda
const Centros = db.models.HOLI_Centros

export {
  Parametros,
  Especialista,
  Categoria,
  Agenda,
  Centros
}
