import Sequelize from 'sequelize'

const db = new Sequelize('HOLI', 'root', 'test', {
  dialect: 'mysql'
})

import {
  Parametros as ParametrosModel,
  Especialista,
  Categoria,
  Agenda,
  Centros
} from './connectors'

const comunas = [
  { id: 1, nombre: 'Santiago' },
  { id: 2, nombre: 'Providencia' },
  { id: 3, nombre: 'Ñuñoa' },
  { id: 4, nombre: 'Peñalolen' },
]

const resolvers = {
  Mutation: {
    creaParametro (_, input) {
      return ParametrosModel.create({
        parId: input.input.parId,
        parCodigo: input.input.parCodigo,
        parDescrip1: input.input.parDescrip1,
        parDescrip2: input.input.parDescrip2,
        parObs: input.input.parObs,
        parOrden: input.input.parOrden,
        parEstado: input.input.parEstado,
        parFechaModif: input.input.parFechaModif,
        parIdModif: input.input.parIdModif
      })
    },
    actualizaParametro (_, input) {
      return ParametrosModel.update({
        parId: input.input.parId,
        parCodigo: input.input.parCodigo,
        parDescrip1: input.input.parDescrip1,
        parDescrip2: input.input.parDescrip2,
        parObs: input.input.parObs,
        parOrden: input.input.parOrden,
        parEstado: input.input.parEstado,
        parFechaModif: input.input.parFechaModif,
        parIdModif: input.input.parIdModif
      }, {
        where: { parIndice: input.indice },
        returning: true
      })
    },
    eliminaParametro (_, input) {
      return ParametrosModel.destroy({
        where: { parIndice: input.parIndice }
      })
    },
    creaEspecialista (_, input) {
      return Especialista.create({
        espRut: input.input.espRut,
        espDv: input.input.espDv,
        espNombres: input.input.espNombres,
        espApePat: input.input.espApePat,
        espApeMat: input.input.espApeMat,
        espFonoCasa: input.input.espFonoCasa,
        espFonoEmpresa: input.input.espFonoEmpresa,
        espCelular: input.input.espCelular,
        espFecNac: input.input.espFecNac,
        espEmail: input.input.espEmail,
        espDireccion: input.input.espDireccion,
        espComuna: input.input.espComuna,
        espObs: input.input.espObs,
        espIdIngresa: input.input.espIdIngresa,
        espFechaIngreso: input.input.espFechaIngreso,
        espContactado: input.input.espContactado,
        espIngresadoDesde: input.input.espIngresadoDesde,
        espUrlFlayer: input.input.espUrlFlayer,
        espPaginaWeb: input.input.espPaginaWeb,
        espEstado: input.input.espEstado
      }, {
        returning: true
      })
    },
    actualizaEspecialista (_, input) {
      return Especialista.update({
        espRut: input.input.espRut,
        espDv: input.input.espDv,
        espNombres: input.input.espNombres,
        espApePat: input.input.espApePat,
        espApeMat: input.input.espApeMat,
        espFonoCasa: input.input.espFonoCasa,
        espFonoEmpresa: input.input.espFonoEmpresa,
        espCelular: input.input.espCelular,
        espFecNac: input.input.espFecNac,
        espEmail: input.input.espEmail,
        espDireccion: input.input.espDireccion,
        espComuna: input.input.espComuna,
        espObs: input.input.espObs,
        espIdIngresa: input.input.espIdIngresa,
        espFechaIngreso: input.input.espFechaIngreso,
        espContactado: input.input.espContactado,
        espIngresadoDesde: input.input.espIngresadoDesde,
        espUrlFlayer: input.input.espUrlFlayer,
        espPaginaWeb: input.input.espPaginaWeb,
        espEstado: input.input.espEstado
      }, {
        where: { espId: input.indice },
        returning: true
      })
    },
    creaAgenda (_, input) {
      return Especialista.find({
        where: { espId: input.indice }
      }).then((esp) => {
        esp.createAgenda({
          espId: esp.espId,
          parIdSemana: input.input.idSemana,
          ageHoraInicio: input.input.horaInicio,
          ageHoraFin: input.input.horaFin,
          ageHoraFlexible: input.input.horaFlexible,
          ageRangoPorDefecto: input.input.rangoPorDefecto,
          vigenciaInicio: input.input.vigenciaInicio,
          vigenciaFin: input.input.vigenciaFin,
          ageEstado: input.input.estado
        })
      })
    }
  },
  Query: {
    async Agenda (_, input) {
      let fecha = typeof input.fecha !== 'undefined' ? `'${input.fecha}'` : 'null'
      let text = typeof input.text !== 'undefined' ? `'%${input.text}%'` : 'null'
      let comuna = typeof input.comuna !== 'undefined' ? input.comuna : 'null'

      let strSql = `
        select
          ag.id,
          eseObs,
          empIngresa,
          eseEstado,
          horaInicio,
          horaFin,
          centro,
          ag.espId,
          ag.catId
        from
          HOLI_Agendas as ag join
          HOLI_CATegoria as cat on ag.catId = cat.catId join
          HOLI_ESPecialistas as esp on esp.espId = ag.espId join
          HOLI_Centros as cen on ag.centro = cen.id
        where
          ag.horaInicio >= ifnull(${fecha}, ag.horaInicio)
          and cen.comuna = ifnull(${comuna}, cen.comuna)
          and cat.catNombre like ifnull(${text}, cat.catNombre)
      `
      let result = await db.query(strSql, { type: Sequelize.QueryTypes.SELECT })
      return result
    },
    allCategoria () {
      return Categoria.findAll()
    },
    AllEspecialistas () {
      return Especialista.findAll()
    },
    getEspecialista (_, input) {
      return Especialista.find({
        where: { espId: input.indice }
      })
    }
  },
  Especialista: {
    id (especialista) {
      return especialista.espId
    },
    rut (especialista) {
      return especialista.espRut
    },
    verificador (especialista) {
      return especialista.espDv
    },
    nombres (especialista) {
      return especialista.espNombres
    },
    apellidopaterno (especialista) {
      return especialista.espApePat
    },
    apellidomaterno (especialista) {
      return especialista.espApeMat
    },
    fonocasa (especialista) {
      return especialista.espFonoCasa
    },
    fonoempresa (especialista) {
      return especialista.espFonoEmpresa
    },
    celular (especialista) {
      return especialista.espCelular
    },
    fechanacimiento (especialista) {
      return especialista.espFecNac
    },
    email (especialista) {
      return especialista.espEmail
    },
    direccion (especialista) {
      return especialista.espDireccion
    },
    comuna (especialista) {
      return especialista.espComuna
    },
    observacion (especialista) {
      return especialista.espObs
    },
    usuario (especialista) {
      return especialista.espIdIngresa
    },
    fechaingreso (especialista) {
      return especialista.espFechaIngreso
    },
    contactado (especialista) {
      return especialista.espContactado
    },
    ingresadodesde (especialista) {
      return especialista.espIngresadoDesde
    },
    urlflayer (especialista) {
      return especialista.espUrlFlayer
    },
    paginaweb (especialista) {
      return especialista.espPaginaWeb
    },
    estado (especialista) {
      return especialista.espEstado
    }
  },
  Categoria: {
    id (categoria) {
      return categoria.catId
    },
    nombre (categoria) {
      return categoria.catNombre
    },
    descripcion (categoria) {
      return categoria.catDescripcion
    },
    icono (categoria) {
      return categoria.catIcono
    },
    usuario  (categoria) {
      return categoria.catIdIngresa
    },
    fechaIngreso (categoria) {
      return categoria.catFechaIngreso
    },
    estado (categoria) {
      return categoria.catEstado
    },
    imagen (categoria) {
      return categoria.catImagen
    }
  },
  Centro: {
    id (centro) {
      return centro.id
    },
    nombre (centro) {
      return centro.nombre
    },
    comuna (centro) {
      return comunas.find((com) => {
        return com.id === centro.id
      })
    },
    direccion (centro) {
      return centro.direccion
    }
  },
  AgendaItem: {
    id (agenda) {
      return agenda.id
    },
    observacion (agenda) {
      return agenda.eseObs
    },
    usuario (agenda) {
      return agenda.empIngresa
    },
    estado (agenda) {
      return agenda.eseEstado
    },
    horainicio (agenda) {
      return agenda.horaInicio
    },
    horafin (agenda) {
      return agenda.horaFin
    },
    centro (agenda) {
      return Centros.find({
        where: { id: agenda.centro }
      })
    },
    categoria (agenda) {
      return Categoria.find({
        where: { catId: agenda.catId }
      })
    },
    especialista (agenda) {
      return Especialista.find({
        where: { espId: agenda.espId }
      })
    },
    precio () {
      return '$ 15.000'
    }
  },
  Comuna: {
    id (comuna) {
      return comuna.id
    },
    nombre (comuna) {
      return comuna.nombre
    }
  }
}

export default resolvers
