const query = `
  type Query {
    AllEspecialistas: [Especialista]
    Agenda(comuna: Int, fecha: String, text: String): [AgendaItem]
    allCategoria: [Categoria]
    getEspecialista (indice: Int): Especialista
  }
`

export default query
