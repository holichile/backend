
const mutation = `
  type Mutation {
    creaParametro(input: ParametroInput): Parametro
    actualizaParametro(indice: Int, input: ParametroInput): Parametro
    eliminaParametro(parIndice: Int): Parametro
    creaEspecialista(input: EspecialistaInput): Especialista
    actualizaEspecialista(indice: Int, input: EspecialistaInput): Parametro
    creaAgenda(indice: Int, input: AgendaInput): AgendaItem
  }
`
export default mutation
