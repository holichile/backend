import Parametros from './parametros'
import Especialistas from './especialistas'
import Categoria from './categoria'
import Querys from './querys'
import Mutations from './mutations'
import Agenda from './agenda'
import Centros from './centro'

const Schema = `
  schema {
    query: Query
    mutation: Mutation
}`

export default [Parametros, Especialistas, Agenda, Categoria, Centros, Querys, Mutations, Schema]
