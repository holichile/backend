const typeDefinitionsCategoria = `
  input CategoriaInput {
    nombre: String
    descripcion: String
    icono: String
    usuario: Int
    fechaIngreso: String
    estado: Int
    imagen: String
  }

  type Categoria {
    id: Int
    nombre: String
    descripcion: String
    icono: String
    usuario: Int
    fechaIngreso: String
    estado: Int
    imagen: String
  }
`

export default typeDefinitionsCategoria
