
const typeDefinitionsEspecialistas = `
  input EspecialistaInput {
    rut: Int
    verificador: String
    nombres: String
    apellidopaterno: String
    apellidomaterno: String
    fonocasa: String
    fonoempresa: String
    celular: String
    fechanacimiento: String
    email: String
    direccion: String
    comuna: Int
    observacion: String
    usuario: Int
    fechaingreso: String
    contactado: Int
    ingresadodesde: String
    urlflayer: String
    paginaweb: String
    estado: Int
  }

  type Especialista {
    id: Int
    rut: Int
    verificador: String
    nombres: String
    apellidopaterno: String
    apellidomaterno: String
    fonocasa: String
    fonoempresa: String
    celular: String
    fechanacimiento: String
    email: String
    direccion: String
    comuna: Int
    observacion: String
    usuario: Int
    fechaingreso: String
    contactado: Int
    ingresadodesde: String
    urlflayer: String
    paginaweb: String
    estado: Int
  }
`
export default typeDefinitionsEspecialistas
