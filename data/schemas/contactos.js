const typeDefinitionsContactos = `
  input ContactoInput {
    conTipoEspPer: Int
    conIdTipo: Int
    parTipo: Int
    conDetalle: String
    empIngresa: Int
    espFechaIngreso: String
    espEstado: Int
  }

  type Contacto {
    conId: Int
    conTipoEspPer: Int
    conIdTipo: Int
    parTipo: Int
    conDetalle: String
    empIngresa: Int
    espFechaIngreso: String
    espEstado: Int
  }
`

export default typeDefinitionsContactos
