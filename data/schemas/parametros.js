const typeDefinitionsParametros = `
input ParametroInput {
  parId: Int
  parCodigo: Int
  parDescrip1: String
  parDescrip2: String
  parObs: String
  parOrden: Int
  parEstado: Int
  parFechaModif: String
  parIdModif: Int
}

type Parametro {
  parIndice: Int
  parId: Int
  parCodigo: Int
  parDescrip1: String
  parDescrip2: String
  parObs: String
  parOrden: Int
  parEstado: Int
  parFechaModif: String
  parIdModif: Int
}`

export default typeDefinitionsParametros
