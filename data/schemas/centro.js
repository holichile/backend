const typeDefinitionsCentros = `
  input CentroInput {
    nombre: String
    comuna: Int
    direccion: String
  }

  type Comuna {
    id: Int
    nombre: String
  }

  type Centro {
    id: Int
    nombre: String
    comuna: Comuna
    direccion: String
  }
`

export default typeDefinitionsCentros
