const typeDefinitionsAgendas = `
  input AgendaInput {
    observacion: String
    usuario: Int
    estado: Int
    horainicio: String
    horafin: String
    centro: Int
    especialista: Int
    categoria: Int
  }

  type AgendaItem {
    id: Int
    observacion: String
    usuario: Int
    estado: Int
    horainicio: String
    horafin: String
    centro: Centro
    categoria: Categoria
    especialista: Especialista
    precio: String
  }
`

export default typeDefinitionsAgendas
