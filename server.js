import express from 'express'
import { apolloServer } from 'graphql-tools'
import schema from './data/schemas/index'
import Resolvers from './data/resolvers'
import cors from 'cors'
const GRAPHQL_PORT = 8080

const graphQLServer = express()

graphQLServer.use(cors())

graphQLServer.use('/graphiql', apolloServer({
  graphiql: true,
  pretty: true,
  schema: schema,
  resolvers: Resolvers,
  allowUndefinedInResolve: true,
  printErrors: true
}))

graphQLServer.use('/graphql', apolloServer({
  graphiql: false,
  pretty: true,
  schema: schema,
  resolvers: Resolvers,
  allowUndefinedInResolve: true,
  printErrors: true
}))

graphQLServer.listen(GRAPHQL_PORT, () => console.log(
  `GraphQL Server is now running on http://localhost:${GRAPHQL_PORT}/graphql`
))
